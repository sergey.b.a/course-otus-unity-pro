# Course --- Otus Unity Pro

My walk through "Unity Game Developer. Professional" course program by OTUS.
PDF with program available here: https://otus.ru/lessons/unity-professional/program/
No educational materials can be found here, only my personal educational practice.
